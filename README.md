# K.C. Noire

**Year:** 2012

**Tools:** C++/Lua

**Notes:** Made for the [Moosader board competition 7](http://www.moosader.com/old-pages/competitions/competition-7-text-based-games/)

## Screenshots

![Screenshot of game](screenshot-kc.png)
