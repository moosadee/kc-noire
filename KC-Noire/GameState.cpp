// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#include "GameState.h"
#include "GameIO.h"
#include "Utils.h"
#include <vector>

bool GameState::Init(lua_State* state)
{
    m_state = state;
    m_location.Init( state );
    m_item.Init( state );
    m_notebook.Init( state );
    m_people.Init( state, m_notebook );
    m_lua.Init( state );
    m_movedLastTurn = true;
    return true;
}

bool GameState::MainLoop()
{
    std::string choice;
    bool commandSuccess = false;
    m_notebook.AddItemToNotebook( "person.richardson", "Richardson" );
    m_notebook.AddItemToNotebook( "location.kenagypark", "Kenagy Park" );
    while ( 1 )
    {
        if ( m_ptrMission->HandleMissionSwitch() )
        {
            // Returns true on quit.
            return false;
        }

        commandSuccess = false;
        if ( m_movedLastTurn )
        {
            DisplayHud();
            m_movedLastTurn = false;
        }

        GIO::Out( "\n\t");
        GIO::Out( m_ptrMission->GetWhatsNextPrompt() );
        GIO::Out( "\n\t>> " );
        choice = GIO::GetUserInput();

        if ( choice == "quit" || choice == "exit" )
        {
            // TODO: Fix state changing to not be based on a bool.
            GIO::OutLine( "\nI had had enough of this bullshit.\nI turned in my badge and gun an' decided to retire t' Florida.\n" );
            return false;
        }
        else
        {
            // Weird code block TODO: cleanup
            commandSuccess =    TryToMove( choice );
            commandSuccess =    ( commandSuccess ) ? commandSuccess : TryToExamine( choice );
            commandSuccess =    ( commandSuccess ) ? commandSuccess : TryToTalk( choice );
            commandSuccess =    ( commandSuccess ) ? commandSuccess : TryNotebook( choice );
            commandSuccess =    ( commandSuccess ) ? commandSuccess : TryLua( choice );

            if ( !commandSuccess )
            {
                GIO::OutLine();
                GIO::OutLine( m_ptrMission->GetBadCommandResponse() );
            }
        }
    }

    return 0; // This shouldn't happen. :P
}

void GameState::SetMissionPtr( Mission& msn )
{
    m_ptrMission = &msn;
}

std::string GameState::Name()
{
    return "Game";
}

bool GameState::TryLua( const std::string& command )
{
    if ( Utils::StringContains( command, "lua" ) )
    {
        m_lua.DoLuaCommand();
        return true;
    }
    return false;
}

bool GameState::TryToMove( const std::string& command )
{
    bool moved = false;
    if ( Utils::StringContains( command, "north" )
        || Utils::StringContains( command, "south" )
        || Utils::StringContains( command, "east" )
        || Utils::StringContains( command, "west" ) )
    {
        moved = m_location.Move( command );
        if ( moved )
        {
            m_movedLastTurn = true;
        }
        else
        {
            GIO::OutLine( "Cannot move " + command + "\n" );
            m_movedLastTurn = false;
        }
    }
    return moved;
}

bool GameState::TryToExamine( const std::string& command )
{
    if ( Utils::StringContains( command, "examine" )
        || Utils::StringContains( command, "look" ) )
    {
        std::vector<std::string> examineCommands;
        examineCommands = Utils::SplitString( command, ' ' );

        if ( examineCommands.size() == 1 )
        {
            // EXAMINE ROOM
            m_location.DisplayLocationInfoBrief();
            m_item.DisplayItemsOnMap( m_location.GetLocationRoomKey() );
            m_people.DisplayPeopleOnMap( m_location.GetLocationRoomKey() );
        }
        else
        {
            std::string examineWord;
            for ( unsigned int i = 1; i < examineCommands.size(); i++ )
            {
                if ( i != 1 ) { examineWord += " "; }
                examineWord += examineCommands[i];
            }

            if ( m_item.ItemExistsAndIsOnMap( examineWord, m_location.GetLocationRoomKey() ) && m_item.DisplayItemDescription( examineWord ) )
            {
                // TODO: Clean up
                m_item.SetItemCollected( examineWord );
                m_notebook.AddItemToNotebook( "item." + m_item.GetItemKey(examineWord), examineWord );
            }
            else if ( m_people.PersonExistsAndIsOnMap( examineWord, m_location.GetLocationRoomKey() ) && m_people.DisplayPersonDescription( examineWord ) )
            {
                m_notebook.AddItemToNotebook( "person." + m_item.GetItemKey(examineWord), examineWord );
            }
            else
            {
                GIO::OutLine( "I looked for a \"" + examineWord + "\" but could not find any." );
            }
        }
        return true;
    }
    return false;
}

bool GameState::TryToTalk( const std::string& command )
{
    std::vector<std::string> talkCommand;
    talkCommand = Utils::SplitString( command, ' ' );
    if ( Utils::StringContains( command, "talk" ) && talkCommand.size() >= 1 )
    {
        std::string talkTo;
        for ( unsigned int i = 1; i < talkCommand.size(); i++ )
        {
            if ( i != 1 ) { talkTo += " "; }
            talkTo += talkCommand[i];

            if ( m_people.StartDialogWith( talkTo, m_location.GetLocationRoomKey() ) )
            {
                GIO::OutLine();
                return true;
            }
            else
            {
                // TODO: If user knows about a person/item, say something different besides "it doesn't exist".
                GIO::OutLine( "I couldn't find a " + talkTo + "." );
                return false;
            }
        }
    }
    return false;
}

bool GameState::TryNotebook( const std::string& command )
{
    if ( Utils::StringContains( command, "notebook" )
        || Utils::StringContains( command, "note" ) )
    {
        m_notebook.DisplayNotebookContents();
    }
    return false;
}

void GameState::DisplayHud()
{
    GIO::ClearScreen();
    GIO::DisplayHorizBar( "COMMANDS" );
    GIO::OutLine( "NORTH, SOUTH, EAST, WEST, EXAMINE [ITEM], TALK [PERSON], NOTEBOOK | QUIT" );
    GIO::DisplayHorizBar();
    m_location.DisplayLocationInfo();
    m_item.DisplayItemsOnMap( m_location.GetLocationRoomKey() );
    m_people.DisplayPeopleOnMap( m_location.GetLocationRoomKey() );
    GIO::DisplayHorizBar();
}

