#ifndef _UTILS
#define _UTILS

#include <vector>
#include <string>
#include <iostream>

namespace Utils
{

std::string SoftwareVersion();

std::vector<std::string> SplitString( const std::string& text, char delim );

std::string ToLower( const std::string& text );

int StringToInt( const std::string& str );

std::string IntToString( int num );

std::string FirstLettersToUpper( const std::string& str );

bool StringContains( const std::string& haystack, const std::string& needle );

std::string GetUserInput();

void Sleep(int milliseconds);

void WaitForEnter();

}

#endif
