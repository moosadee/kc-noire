// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#include "StateManager.h"

StateManager::StateManager()
{
    m_state = lua_open();
    luaL_openlibs( m_state );
    luabind::open( m_state );
    Init();
}

StateManager::~StateManager()
{
    lua_close( m_state );
}

void StateManager::Init()
{
    m_menuState.Init( m_state );
    m_gameState.Init( m_state );
    m_currentState = &m_menuState;
    m_mission.Init( m_state );
    m_menuState.SetMissionPtr( m_mission );
    m_gameState.SetMissionPtr( m_mission );
}

void StateManager::MainLoop()
{
    bool playing = true;

    while ( playing )
    {
        playing = m_currentState->MainLoop();

        if ( playing && m_currentState->Name() == "Menu" )
        {
            // Menu state has returned, go to game state now.
            // This could be handled better, but right now it's fine.
            m_currentState = &m_gameState;
        }
    }
}
