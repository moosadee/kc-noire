// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#ifndef _ITEM
#define _ITEM

#include "ItemWrapper.h"

class Item
{
    public:
    void Init( lua_State* state );
    void DisplayItemsOnMap( const std::string& locRoomKey );
    bool DisplayItemDescription( const std::string& itemName );
    bool SetItemCollected( const std::string& itemName );
    std::string GetItemKey( const std::string& itemName );
    bool ItemExistsAndIsOnMap( const std::string& itemName, const std::string& mapKey );

    private:
    ItemWrapper m_wItem;
};

#endif
