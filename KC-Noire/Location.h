// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#ifndef _LOCATION
#define _LOCATION

#include "LocationWrapper.h"

class Location
{
    public:
    void Init(lua_State* state);
    void DisplayLocationInfo();
    std::vector<std::string> GetNeighborRoomInfo();
    bool Move(const std::string& direction);
    std::string GetLocationRoomKey();
    void DisplayLocationInfoBrief();
    bool HandleCarTransportation();
    bool MoveToLocationByName( const std::string locName );

    private:
    LocationWrapper m_wLoc;
};

#endif

// EOF
