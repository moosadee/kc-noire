// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#include "GameIO.h"
#include "Utils.h"
#include <cstdlib>
#include <fstream>
#include <ctime>

void GIO::Init()
{
    InitIOLog();
}

void GIO::Close()
{
    CloseIOLog();
}

void GIO::Out( std::string text )
{
    std::cout << text;
    m_OutStream << text;
}

void GIO::OutLine( std::string text )
{
    std::cout << text << std::endl;
    m_OutStream << text << std::endl;
}

void GIO::Type( const std::string& text)
{
    for ( unsigned int i = 0; i < text.size(); i++ )
    {
        std::cout << text[i] << std::flush;
        Utils::Sleep( m_TypeSpeedMs );
    }
    m_OutStream << text;
}

void GIO::Debug( const std::string& text )
{
    if ( IN_GAME_DEBUG )
    {
        std::cout << "** DEBUG ** " << text << "** END DEBUG ** " << std::endl;
    }
    m_DebugStream << text << std::endl;
}

void GIO::Err( const std::string& text )
{
    if ( IN_GAME_DEBUG )
    {
        std::cout << "** ERROR ** " << text << "** END ERROR ** " << std::endl;
    }
    m_DebugStream << text << std::endl;
}

void GIO::DisplayHorizBar( std::string header )
{
    int length = 80;
    if ( header != "" )
    {
        length = 80 - header.size() - 3;
        std::cout << "- " << header << " ";
        m_OutStream << "- " << header << " ";
    }
    for ( int i = 0; i < length; i++ )
    {
        std::cout << "-";
        m_OutStream << "-";
    }
    OutLine();
}

void GIO::ClearScreen()
{
    if ( !IN_GAME_DEBUG )
    {
    #ifdef __MINGW32__
        system("cls");
    #endif

    #ifdef __GNUC__
        system("clear");
    #endif
    }
}

std::string GIO::GetUserInput()
{
    std::string buffer;
    getline( std::cin, buffer );
    OutLine();
    m_OutStream << "** USER INPUT: " + buffer << " **\n";
    return buffer;
}

int GIO::GetUserInputInt()
{
    std::string buffer;
    getline( std::cin, buffer );
    OutLine();
    m_OutStream << "** USER INPUT (INT): " + buffer << " **\n";
    return Utils::StringToInt( buffer );
}

void GIO::SetTypeSpeed( int ms )
{
    GIO::Debug( "Setting type speed to: " + Utils::IntToString( ms ) + " (ms)" );
    m_TypeSpeedMs = ms;
}

void GIO::InitIOLog()
{
    std::string gameLogName = "GameLog ";
    std::string debugLogName = "DebugLog ";

    std::time_t curTime;
    std::time( &curTime );
    gameLogName += ctime( &curTime );
    debugLogName += ctime( &curTime );

    gameLogName += ".txt";
    debugLogName += ".txt";

    std::string gameLogPath = "logs/" + gameLogName;
    std::string debugLogPath = "logs/" + debugLogName;
    m_OutStream.open( gameLogPath.c_str() );
    m_DebugStream.open( debugLogPath.c_str() );
}

void GIO::CloseIOLog()
{
    DisplayHorizBar();
    OutLine( "Thanks for playing!" );
    OutLine( "Make sure to check out the \"log\" folder and send your log to:\n RachelJMorris@gmail.com !" );
    m_OutStream << "\nHey, you there. I created this logging thing for a reason!";
    m_OutStream << "\nIt'd be really cool to receive an email with this log in it,";
    m_OutStream << "\nto see how you played the game!";
    DisplayHorizBar();

    m_DebugStream << "\nIf you had any problems playing this game,";
    m_DebugStream << "\nSend this log to RachelJMorris@gmail.com";

    m_OutStream.close();
    m_DebugStream.close();
}

// EOF
