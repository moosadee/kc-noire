// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#include "MissionWrapper.h"
#include <iostream>
#include <string>
#include <vector>
#include "Utils.h"
#include "GameIO.h"

void MissionWrapper::Init(lua_State* state)
{
    BaseLuaWrapper::Init(state, "missions");
}

std::string MissionWrapper::GetNameOfCurrentMission()
{
    GIO::Debug( "GetNameOfCurrentMission()" );
    std::string result;
    try { result = luabind::call_function<std::string>( ptrState, "mission_GetNameOfCurrent" ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + " M1" ); }
    GIO::Debug( "Result " + result );
    return result;
}

std::vector<std::string> MissionWrapper::GetListOfMissions()
{
    GIO::Debug( "GetListOfMissions()" );
    std::string result;
    try { result = luabind::call_function<std::string>( ptrState, "mission_GetListOfMissions" ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + " M2" ); }
    std::vector<std::string> lstElements = Utils::SplitString(result, '|');
    GIO::Debug( "Result " + result );
    return lstElements;
}

std::string MissionWrapper::GetMissionIntroduction()
{
    GIO::Debug( "GetMissionIntroduction()" );
    std::string result;
    try { result = luabind::call_function<std::string>( ptrState, "mission_GetMissionIntroduction" ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + " M3" ); }
    GIO::Debug( "Result " + result );
    return result;
}

bool MissionWrapper::SetCurrentMission( const std::string& key )
{
    GIO::Debug( "SetCurrentMission(" + key + ")" );
    bool result = false;
    try { result = luabind::call_function<bool>( ptrState, "mission_SetCurrentMission", key ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + " M4" ); }
    GIO::Debug( "Result " + result );
    return result;
}

std::string MissionWrapper::GetBadCommandResponse()
{
    GIO::Debug( "GetBadCommandResponse()" );
    std::string result;
    try { result = luabind::call_function<std::string>( ptrState, "mission_GetBadCommandResponse" ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + " M5" ); }
    GIO::Debug( "Result " + result );
    return result;
}

std::string MissionWrapper::GetWhatsNextPrompt()
{
    GIO::Debug( "GetWhatsNextPrompt()" );
    std::string result;
    try { result = luabind::call_function<std::string>( ptrState, "mission_GetWhatsNext" ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + " M6" ); }
    GIO::Debug( "Result " + result );
    return result;
}

std::string MissionWrapper::GetMissionEndFlag()
{
    GIO::Debug( "GetMissionEndFlag()" );
    std::string result;
    try { result = luabind::call_function<std::string>( ptrState, "mission_CheckForEndFlag" ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + " M7" ); }
    GIO::Debug( "Result " + result );
    return result;
}

std::vector<std::string> MissionWrapper::GetEndMissionDialog( const std::string& switchVal )
{
    GIO::Debug( "GetEndMissionDialog(" + switchVal + ")" );
    std::string result;
    try { result = luabind::call_function<std::string>( ptrState, "mission_GetEndMissionDialog", switchVal ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + "M8" ); }
    std::vector<std::string> lstElements = Utils::SplitString(result, '|');
    GIO::Debug( "Result " + result );
    return lstElements;
}

// EOF

