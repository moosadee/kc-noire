// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#ifndef _GAMESTATE
#define _GAMESTATE

#include "State.h"
#include "Item.h"
#include "Location.h"
#include "Notebook.h"
#include "Mission.h"
#include "People.h"
#include "LuaDebugWrapper.h"

#include <string>

class GameState : public State
{
    public:
    virtual bool Init(lua_State* state);
    virtual bool MainLoop();
    virtual std::string Name();
    void SetMissionPtr( Mission& msn );

    private:
    Location m_location;
    Item m_item;
    Notebook m_notebook;
    People m_people;
    Mission* m_ptrMission;
    LuaDebugWrapper m_lua;
    bool m_movedLastTurn;

    void DisplayHud();
    bool TryLua( const std::string& command );
    bool TryToMove( const std::string& command );
    bool TryToExamine( const std::string& command );
    bool TryToTalk( const std::string& command );
    bool TryNotebook( const std::string& command );
};

#endif
