// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#include "LuaDebugWrapper.h"
#include "GameIO.h"
#include "Utils.h"
#include <vector>
#include <lua.hpp>

void LuaDebugWrapper::Init(lua_State* state)
{
    BaseLuaWrapper::Init(state);
}

void LuaDebugWrapper::DoLuaCommand()
{
    GIO::DisplayHorizBar( "LUA DEBUG START" );
    GIO::OutLine();

    std::string command = "";
    GIO::Out( "Enter Lua command, or \"exit\" to stop: " );
    GIO::OutLine();
    while ( !Utils::StringContains( command, "exit" ) )
    {
        GIO::OutLine();
        GIO::Out( ">> ");
        command = GIO::GetUserInput();

        // Props to TheBuzzSaw for the help w/ this
        int status = luaL_loadstring( ptrState, command.c_str() );
        if ( status )
        {
            GIO::Debug( "Lua status: " + status );
        }
        else
        {
            status = lua_pcall( ptrState, 0, LUA_MULTRET, 0 );
            GIO::Debug( "Lua status: " + status );
        }
    }

    GIO::OutLine();
    GIO::DisplayHorizBar( "LUA DEBUG END" );
}
