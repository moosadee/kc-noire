// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#ifndef _GAMEOUTPUT
#define _GAMEOUTPUT

#include <iostream>

// Gameplay output will use these functions so I can do any necessary
// formatting or other things later.

namespace Go // "Game Output"
{
    void Out( std::string text );
    void OutLine( std::string text = "" );
    void Type( std::string text );
    void DisplayHorizBar();
    void ClearScreen();
}

#endif
