// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#ifndef _ITEMWRAPPER
#define _ITEMWRAPPER

#include <string>
#include "BaseLuaWrapper.h"

class ItemWrapper : public BaseLuaWrapper
{
    public:
    void Init(lua_State* state);

    std::string GetDescriptionOfItem( const std::string& itemName );
    std::string GetNameOfItem( const std::string& itemKey );
    bool SetItemCollected( const std::string& itemName );
    std::vector<std::string> GetListOfItemsOnMap( const std::string& mapKey );
    std::string GetItemKeyFromName( const std::string& itemName );
    bool ItemExistsAndIsOnMap( const std::string& itemName, const std::string& mapKey );
};

#endif
