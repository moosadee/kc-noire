// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#ifndef _PEOPLE
#define _PEOPLE

#include "PeopleWrapper.h"
#include "Notebook.h"

enum DialogType { TALK, ASK, INTERROGATION, EVIDENCE, END };

class People
{
    public:
    void Init(lua_State* state, Notebook& notebook);
    void DisplayPeopleOnMap( const std::string& locRoomKey );
    bool DisplayPersonDescription( const std::string& personName );
    bool StartDialogWith( const std::string& personName, const std::string& mapKey );
    bool PersonExistsAndIsOnMap( const std::string& personName, const std::string& mapKey );

    private:
    PeopleWrapper m_wPeople;
    DialogType m_dialogType;
    Notebook* m_ptrNotebook;
    std::string m_itemSubject;

    void DisplayDialog( const std::vector<std::string> lstDialog );
    void ProcessDialog( const std::string& personName, const std::string& state, const std::string& message = "" );
    std::string GetDialogResponse( const std::string& personName );
};

#endif
