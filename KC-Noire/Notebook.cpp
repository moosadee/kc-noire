// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#include "Notebook.h"
#include "Utils.h"
#include "GameIO.h"

void Notebook::Init(lua_State* state)
{
    m_wNotebook.Init( state );
}

void Notebook::DisplayNotebookLocations()
{
    GIO::OutLine( "\nLocations" );
    std::vector<std::string> lstEntries = GetAllNotebookEntries();
    std::string entryName;
    for ( unsigned int i = 0; i < lstEntries.size(); i++ )
    {
        if ( Utils::StringContains( lstEntries[i], "location." ) )
        {
            entryName = Utils::SplitString( lstEntries[i], '=' )[1];
            GIO::OutLine( "* " + entryName );
        }
    }
}

void Notebook::DisplayNotebookEvidence()
{
    GIO::OutLine( "\nEvidence" );
    std::vector<std::string> lstEntries = GetAllNotebookEntries();
    std::string entryName;
    for ( unsigned int i = 0; i < lstEntries.size(); i++ )
    {
        if ( Utils::StringContains( lstEntries[i], "item." )
            || Utils::StringContains( lstEntries[i], "person." ) )
        {
            entryName = Utils::SplitString( lstEntries[i], '=' )[1];
            GIO::OutLine( "* " + entryName );
        }
    }
}

void Notebook::DisplayNotebookContents()
{
    GIO::DisplayHorizBar( "NOTEBOOK" );
    DisplayNotebookEvidence();
    DisplayNotebookLocations();
    GIO::OutLine();
    GIO::DisplayHorizBar();
}

void Notebook::AddItemToNotebook( const std::string& entryTypeAndKey, const std::string& entryName )
{
    bool success = m_wNotebook.AddItemToNotebook( entryTypeAndKey, entryName );
    if ( success )
    {
        GIO::Type( "\nAdded " + entryName + " to my Notebook." );
    }
    else
    {
        GIO::Type( "\nFailed to add to notebook" );
    }
    GIO::OutLine();
}

std::vector<std::string> Notebook::GetAllNotebookEntries()
{
    return m_wNotebook.GetNotebookContents();
}

std::vector<std::string> Notebook::GetNotebookDialogEntries( const std::string& talkingToName )
{
    return m_wNotebook.GetNotebookContentsForDialog( talkingToName );
}








