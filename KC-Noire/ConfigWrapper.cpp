// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#include "ConfigWrapper.h"
#include "GameIO.h"

void ConfigWrapper::Init(lua_State* state)
{
    BaseLuaWrapper::Init(state, "gameconfig");
}

int ConfigWrapper::GetTypeSpeed()
{
    GIO::Debug( "GetTypeSpeed()" );
    int result;
    try { result = luabind::call_function<int>( ptrState, "config_GetTypeSpeed" ); }
    catch( luabind::error e ) { GIO::Err( "\nERROR: " + std::string(std::string(lua_tostring( ptrState, -1 ))) + " C1" ); }
    GIO::Debug( "Result " + result );
    return result;
}
