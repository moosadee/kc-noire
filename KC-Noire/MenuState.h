// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#ifndef _MENUSTATE
#define _MENUSTATE

#include "State.h"
#include "Mission.h"
#include "ConfigWrapper.h"

class MenuState : public State
{
    public:
    virtual bool Init(lua_State* state);
    virtual bool MainLoop();
    virtual std::string Name();
    void SetMissionPtr( Mission& msn );

    private:
    Mission* m_ptrMission;
    ConfigWrapper m_wConfig;

    void DisplayTitleScreen();
};

#endif
