// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#include "Utils.h"
#include <sstream>
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>

namespace Utils
{

// This is where the version # is defined.
std::string SoftwareVersion()
{
    return "1.0 - 2012-04-08";
}

std::vector<std::string> SplitString( const std::string& text, char delim )
{
    std::vector<std::string> lstStrings;

    unsigned int begin = 0;
    for ( unsigned int i = 0; i < text.size(); i++ )
    {
        if ( text[i] == delim && i == begin )
        {
            begin++;
        }
        else if ( text[i] == delim )
        {
            lstStrings.push_back( text.substr( begin, i - begin ) );

            begin = i+1; // Start after this delimiter
        }

        if ( i == text.size() - 1 && text[i] != delim )
        {
            lstStrings.push_back( text.substr( begin, text.size() - begin ) );
        }
    }

    return lstStrings;
}

std::string FirstLettersToUpper( const std::string& str )
{
    // TODO
    return str;
}

std::string ToLower( const std::string& text )
{
    std::string lower = "";
    for ( unsigned int i = 0; i < text.size(); i++ )
    {
        lower += tolower(text[i]);
    }
    return lower;
}

int StringToInt( const std::string& str )
{
    return atoi( str.c_str() );
}

std::string IntToString( int num )
{
    std::stringstream ss;
    ss << num;
    return ss.str();
}

bool StringContains( const std::string& haystack, const std::string& needle )
{
    size_t notFoundLen = std::string::npos;
    return ( haystack.find( needle ) != notFoundLen );
}

void Sleep(int milliseconds)
{
    #ifdef __MINGW32__
        sleep( milliseconds );
    #endif

    #ifdef __GNUC__
        usleep( milliseconds );
    #endif
}

void WaitForEnter()
{
    while ( 1 ) { if ( '\n' == getchar() ) { break; } }
}

}
