#include "Item.h"
#include "Utils.h"
#include "GameIO.h"

void Item::Init(lua_State* state)
{
    m_wItem.Init( state );
}

void Item::DisplayItemsOnMap( const std::string& locRoomKey )
{
    std::vector<std::string> lstItems = m_wItem.GetListOfItemsOnMap(locRoomKey);

    GIO::OutLine();
    for ( unsigned int i = 0; i < lstItems.size(); i++ )
    {
        GIO::OutLine( "There was a " + m_wItem.GetNameOfItem( lstItems[i] ) + " here." );
    }
}

bool Item::DisplayItemDescription( const std::string& itemName )
{
    std::string description = m_wItem.GetDescriptionOfItem( itemName );
    if ( description == "" ) return false;
    GIO::Type( description );
    return true;
}

bool Item::SetItemCollected( const std::string& itemName )
{
    return m_wItem.SetItemCollected(itemName);
}

std::string Item::GetItemKey( const std::string& itemName )
{
    return m_wItem.GetItemKeyFromName( itemName );
}

bool Item::ItemExistsAndIsOnMap( const std::string& itemName, const std::string& mapKey )
{
    return m_wItem.ItemExistsAndIsOnMap( itemName, mapKey );
}
