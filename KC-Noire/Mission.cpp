// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#include "Mission.h"
#include "Utils.h"
#include "GameIO.h"

void Mission::Init(lua_State* state)
{
    m_wMission.Init( state );
}

int Mission::DisplayMissionList()
{
    std::vector<std::string> lstMissions = m_wMission.GetListOfMissions();

    std::string list;
    for ( unsigned int i = 0; i < lstMissions.size(); i++ )
    {
        list = Utils::IntToString( i+1 );
        list += ".\t" + lstMissions[i];
        GIO::OutLine( list );
    }
    return lstMissions.size();
}

bool Mission::SelectMission( int episode )
{
    std::string missionName = "Mission";
    missionName += episode;
    return m_wMission.SetCurrentMission( missionName );
}

void Mission::DisplayMissionIntroduction()
{
    GIO::ClearScreen();
    GIO::DisplayHorizBar();
    std::string intro = m_wMission.GetMissionIntroduction();
    GIO::Type(intro);
    GIO::OutLine();
    GIO::DisplayHorizBar();
}

std::string Mission::GetBadCommandResponse()
{
    return m_wMission.GetBadCommandResponse();
}

std::string Mission::GetWhatsNextPrompt()
{
    return m_wMission.GetWhatsNextPrompt();
}

bool Mission::HandleMissionSwitch()
{
    std::string missionSwitch = m_wMission.GetMissionEndFlag();
    if ( missionSwitch != "" )
    {
        GIO::DisplayHorizBar( "Mission End... " );

        std::vector<std::string> endingDialog;
        endingDialog = m_wMission.GetEndMissionDialog( missionSwitch );

        for ( unsigned int i = 0; i < endingDialog.size(); i++ )
        {
            GIO::Type( "\n" + endingDialog[i] );
        }

        GIO::Type( "\n\nThe End" );

        GIO::Type( "\nPress ENTER to Quit." );

        Utils::WaitForEnter();
        return true;
    }

    return false;
}

// EOF
