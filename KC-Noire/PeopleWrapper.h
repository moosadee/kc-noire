// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#ifndef _PEOPLEWRAPPER
#define _PEOPLEWRAPPER

#include <string>
#include "BaseLuaWrapper.h"

class PeopleWrapper : public BaseLuaWrapper
{
    public:
    void Init(lua_State* state);

    std::vector<std::string> GetListOfPeopleOnMap( const std::string& mapKey );
    std::string GetNameOfPerson( const std::string& personKey );
    std::string GetDescriptionOfPerson( const std::string& personName );
    std::vector<std::string> GetDialog( const std::string& personName, const std::string& dialogState, const std::string& messageTo = "", const std::string& itemName = "" );
    bool PersonExistsAndIsOnMap( const std::string& personName, const std::string& mapKey );
};

#endif
