-- KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

-- Items are relatively basic in this game; They have a name, description,
-- a Location it belongs to, and a boolean "isCollected".
-- In KC Noire, you don't have an inventory, but when you "Examine" an item,
-- it becomes "Collected" and stored in your Notebook, so you can
-- use information about the item in dialog with people.

-- ITEMS --
item = {}

item.CrumpledPaper = {
    m_name 		    = "Crumpled Paper",
    m_description   = "I picked up and straighten out the crumpled, wet piece of paper.\nIt had appeared to be a recipe for 'Surreptitious Smokey Sexy Sausage BBQ'.",
    m_onMap 		= "kenagypark.parkingLot",
    m_isCollected   = false
}

item.Smoker = {
    m_name 		    = "Smoker",
    m_description   = "I glanced over the smoker. \nIt had obviously been used earlier today, but was currently cold.",
    m_onMap 		= "kenagypark.grassyField",
    m_isCollected   = false
}

-- FUNCTIONALITY --

function item_ItemExistsAndIsOnMap( itemName, mapKey )
    itemObj = item_GetItemTableFromName( itemName )

    if itemObj == nil then return false end
    if itemObj.m_onMap ~= mapKey then return false end
    return true
end

function item_GetItemTableFromName(itemName)
    for key, value in pairs(item) do
        if string.lower(item[key].m_name) == string.lower(itemName) then
            return item[key]
        end
    end
    return nil
end

function item_GetDescription(itemKey)
 	return item[itemKey].m_description
end

function item_GetDescriptionFromName(itemName)
    itemObj = item_GetItemTableFromName(itemName)
    if itemObj ~= nil then
        return itemObj.m_description
    end
    return ""
end

function item_GetKeyFromName( itemName )
    for key, value in pairs(item) do
        if string.lower(item[key].m_name) == string.lower(itemName) then
            return key
        end
    end
    return ""
end

function item_GetName(itemKey)
 	return item[itemKey].m_name
end

function item_SetItemCollected(itemName)
    itemTable = item_GetItemTableFromName(itemName)
    if itemTable ~= nil then
        itemTable.m_isCollected = true
        return true
    end
    return false
end

function item_GetItemsOnMap(mapKey)
    itemList = "|"
	for key, value in pairs(item) do
        if value.m_onMap == mapKey then
            itemList = itemList .. key .. "|"
        end
	end
	return itemList
end

