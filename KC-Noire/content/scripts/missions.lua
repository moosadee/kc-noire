-- KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

-- MISSIONS --

missions = {}

--missions.Mission1 = {
--    m_title = "Grandview Triangle Tragedy",
--    m_introduction = "",
--    m_startingLocation = ""
--}
--
--missions.Mission2 = {
--    m_title = "Shuttlecock Swindler",
--    m_introduction = "",
--    m_startingLocation = ""
--}

missions.Mission3 = {
    m_title = "Barbecue Butchery",
    m_introduction = "I was called to a local park to investigate a murder.\nIt was a week before some barbecue cookoff, I don't know, \nI'm not big on that stuff.\nAnyway, the report said that this guy Richardson had died, \ntrying a test batch of his own meat.\n\nVoice in my head, stop looking at me that way. Ya' know what I mean...\n\n...Barbecued sausage...\n\nNo, seriously, stop that, lil' Brain-Frank.\n\nThe autopsy showed rat poison. \nEither this guy blindly picked up the jar instead-a' the chili sauce...\n\n...or he was murdered...",
    m_startingLocation = "location.kenagypark"
}

startMission = "Mission3";
currentMission = "Mission3";

-- FUNCTIONALITY --

function mission_GetMissionIntroduction()
    return missions[currentMission].m_introduction;
end

function mission_SetCurrentMission(key)
    if missions[key] ~= nil then
        currentMission = key
        return true
    end
    return false
end

function mission_GetNameOfCurrent()
    return missions[startMission].m_title;
end

function mission_GetListOfMissions()
    missionList = "|"
    for key, value in pairs(missions) do
        missionList = missionList .. value.m_title .. "|"
    end
    return missionList;
end

function mission_CheckForEndFlag()
    if endingFlag ~= nil then
        return endingFlag
    end
    return ""
end

function mission_GetEndMissionDialog( switch )
    dlgList = "|"
    for k,v in pairs( missionEnding[switch] ) do
        dlgList = dlgList .. v .. "|"
    end
    return dlgList
end

missionEnding = {
    ["goodEnding"] = {
        "An’ there it was...\n",
        "I took Whited into custody, an’ he’s waiting to be tried for murder.\n",
        "Sometimes, people wrong ya’. They hurt you, or steal somethin’ of value to ya.",
        "Killin’ ain’t the answer, though.",
        "This guy, an amateur.  Can’t keep that stuff hidden from ol’ Frank.\n",
        "…\n",
        "I couldn’t help but look up into the sky.",
        "This case was pretty easy. Was lucky. Lucky for me.",
        "I’m sure my old man was up there watchin’ somewhere.  Sandin’ th’ Moon."
    },
    ["badEnding"] = {
        "After drivin’ off, Whited made his escape.",
        "I was a fool, I shoulda’ seen it.  It was Whited. Whited killed Richardson.",
        "But why?  I didn’t get the chance to find that out..."
    }
}


whatsNext = {
    "I figured the next thing I oughta do was...",                                                              -- 1
    "I tipped my hat and thought about my next move...",                                                        -- 2
    "I kept thinking about shuttlecocks, but I hadta stop.\n\tClear yer mind... On to the next thing...",          -- 3
    "My nose itched. I wiggled it to no avail.\n\tScratching it, it gave me enough time to decide to...",          -- 4
    "Grumbling, I decided to...",                                                                               -- 5
    "Ignoring my pounding headache from my hangover, I could only think of...",                                 -- 6
    "If I were a cat, I would...",                                                                              -- 7
    "Sighing, my next move was..."                                                                              -- 8
}

-- Extra - What gets outputted when your command doesn't make sense --

badCommand = {
    "I checked my surroundings once more...",                                                                   -- 1
    "Random things started popping into my mind.\n\tI tried to clear my head and figured out something to do...", -- 2
    "Suddenly, my craving for cheesecake made me forget what I was doing...",                                   -- 3
    "I started pondering the meaning of life...",                                                               -- 4
    "I lost my concentration because I thought of this great sitcom idea...",                                   -- 5
    "Keep it together, man!",                                                                                   -- 6
    "I wonder what it's like to travel in outer space..."                                                       -- 7
}

-- Extra - If your command contains this, then... --

specificCommands = {}
specificCommands["pick nose"] = {
    m_response = "I'm not a thief, so I was able to safely pick my nose."
}
specificCommands["fuck"] = {
    m_response = "While it sounded good in my head, I decided that it would be very unprofessional."
}
specificCommands["error"] = {
    m_response = "What do you mean? There are no errors in this game!!"
}

function mission_GetWhatsNext()
    size = 8
    cmdIdx = math.random(1, size)
    return whatsNext[cmdIdx]
end

function mission_GetBadCommandResponse()
    size = 7
    cmdIdx = math.random(1, size)
    return badCommand[cmdIdx]
end

function mission_GetSpecificCommandResponse( command )
    return specificCommands[command].m_response
end
