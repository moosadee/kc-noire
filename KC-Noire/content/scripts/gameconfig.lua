-- KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

typeSpeedMs = 10000
-- typeSpeedMs = 0 -- No typing!!

function config_GetTypeSpeed()
    return typeSpeedMs
end
