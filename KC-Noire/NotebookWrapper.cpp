// KC NOIRE - RACHEL J. MORRIS, 2012 - WWW.MOOSADER.COM - GNU GPL V3

#include "Utils.h"
#include "NotebookWrapper.h"
#include "GameIO.h"

void NotebookWrapper::Init(lua_State* state)
{
    BaseLuaWrapper::Init(state, "notebook");
}

bool NotebookWrapper::AddItemToNotebook( const std::string& entryTypeAndKey, const std::string& entryName )
{
    GIO::Debug( "IsItemInNotebook(" + entryTypeAndKey + ", " + entryName + ")" );
    bool result = false;
    try { result = luabind::call_function<bool>( ptrState, "book_AddItemToNotebook", entryTypeAndKey, entryName ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + " B1" ); }
    GIO::Debug( "Result " + result );
    return result;
}

std::vector<std::string> NotebookWrapper::GetNotebookContents()
{
    GIO::Debug( "GetNotebookContents()" );
    std::string result;
    try { result = luabind::call_function<std::string>( ptrState, "book_GetNotebookContents" ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + " B2" ); }
    std::vector<std::string> lstItems = Utils::SplitString(result, '|');
    GIO::Debug( "Result " + result );
    return lstItems;
}

bool NotebookWrapper::IsItemInNotebook( const std::string& entryTypeAndKey )
{
    GIO::Debug( "IsItemInNotebook(" + entryTypeAndKey + ")" );
    bool result = false;
    try { result = luabind::call_function<bool>( ptrState, "book_IsItemInNotebook", entryTypeAndKey ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + " B3" ); }
    GIO::Debug( "Result " + result );
    return result;
}

std::vector<std::string> NotebookWrapper::GetNotebookContentsForDialog( const std::string& talkingToName )
{
    GIO::Debug( "GetNotebookContentsForDialog(" + talkingToName + ")" );
    std::string result;
    try { result = luabind::call_function<std::string>( ptrState, "book_GetNotebookDialogContents", talkingToName ); }
    catch( luabind::error e ) { GIO::Err(  "\nERROR: " + std::string(lua_tostring( ptrState, -1 )) + " B4" ); }
    std::vector<std::string> lstItems = Utils::SplitString(result, '|');
    GIO::Debug( "Result " + result );
    return lstItems;
}
